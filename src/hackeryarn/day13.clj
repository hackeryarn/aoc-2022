(ns hackeryarn.day13
  (:require [clojure.string :as str]))

(defn compare-values [left right]
  (cond
    (and (number? left) (number? right))
    (compare left right)

    (number? left) (recur [left] right)

    (number? right) (recur left [right])

    :else (or
           (->> (map compare-values left right)
                (drop-while zero?)
                first)
           (- (count left) (count right)))))

(defn part1 [input]
  (let [divider-packets #{[[2]] [[6]]}]
    (->> (str/split input #"\n+")
         (map read-string)
         (concat [[[2]] [[6]]])
         (sort compare-values)
         (keep-indexed (fn [index packet]
                         (when (divider-packets packet)
                           (inc index))))
         (apply *))))

(def test-input
  "[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]")

;; (part1 test-input)

(part1 (slurp "resources/day13.txt"))
