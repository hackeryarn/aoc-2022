(ns hackeryarn.day12
  (:require [clojure.string :as str]))

(defn part1 [input]
  (let [width (inc (count (take-while #(not= % \newline) input)))
        terrain (str (apply str (repeat width "-"))
                     (str/replace input #"\n" "-")
                     (apply str (repeat width "-")))
        directions [-1 1 (- width) width]
        end (str/index-of terrain \E)
        terrain (-> terrain
                    (str/replace \S \a)
                    (str/replace \E \z)
                    (. getBytes))
        visited (byte \-)]
    (loop [hikers [[end (byte \z) 0]]]
      (let [hikers (for [[position height distance] hikers
                         :when (if (not= (aget terrain position) visited)
                                 (aset terrain position visited))
                         :let [min-height (dec height)
                               distance (inc distance)]
                         direction directions
                         :let [neighbour (+ position direction)
                               height (aget terrain neighbour)]
                         :when (>= height min-height)]
                     [neighbour height distance])
            [winner] (filterv
                      (fn [[position height distance]]
                        (= height (byte \a)))
                      hikers)]
        (if winner
          ((fn [[position height distance]] distance) winner)
          (recur hikers))))))


(def test-input
  "Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi")

;; (part1 test-input)

(part1 (slurp "resources/day12.txt"))
