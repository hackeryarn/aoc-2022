(ns hackeryarn.day9
  (:require [hackeryarn.utils :as u]
            [clojure.string :as str]))

(def starting-state
  {:head {:x 0 :y 0}
   :tail {:x 0 :y 0}
   :visited #{}})

(defn update-state [state {:keys [head-x head-y tail-x tail-y]}]
  (-> state
      (assoc-in [:head :x] head-x)
      (assoc-in [:head :y] head-y)
      (assoc-in [:tail :x] tail-x)
      (assoc-in [:tail :y] tail-y)
      (update :visited #(conj % (str "x" tail-x "y" tail-y)))))

(defn move-up [{:keys [head tail] :as state}]
  (let [{head-x :x head-y :y} head
        {tail-x :x tail-y :y} tail
        new-head-y (+ 1 head-y)]
    (if (> (- new-head-y tail-y) 1)
      (if (= head-x tail-x)
        (update-state state
                      {:head-x head-x
                       :head-y new-head-y
                       :tail-x tail-x
                       :tail-y (+ 1 tail-y)})
        (update-state state
                      {:head-x head-x
                       :head-y new-head-y
                       :tail-x head-x
                       :tail-y (+ 1 tail-y)}))
      (update-state state
                    {:head-x head-x
                     :head-y new-head-y
                     :tail-x tail-x
                     :tail-y tail-y}))))

(defn move-right [{:keys [head tail] :as state}]
  (let [{head-x :x head-y :y} head
        {tail-x :x tail-y :y} tail
        new-head-x (+ 1 head-x)]
    (if (> (- new-head-x tail-x) 1)
      (if (= head-y tail-y)
        (update-state state
                      {:head-x new-head-x
                       :head-y head-y
                       :tail-x (+ 1 tail-x)
                       :tail-y tail-y})
        (update-state state
                      {:head-x new-head-x
                       :head-y head-y
                       :tail-x (+ 1 tail-x)
                       :tail-y head-y}))
      (update-state state
                    {:head-x new-head-x
                     :head-y head-y
                     :tail-x tail-x
                     :tail-y tail-y}))))

(defn move-down [{:keys [head tail] :as state}]
  (let [{head-x :x head-y :y} head
        {tail-x :x tail-y :y} tail
        new-head-y (- head-y 1)]
    (if (> (- tail-y new-head-y) 1)
      (if (= head-x tail-x)
        (update-state state
                      {:head-x head-x
                       :head-y new-head-y
                       :tail-x tail-x
                       :tail-y (- tail-y 1)})
        (update-state state
                      {:head-x head-x
                       :head-y new-head-y
                       :tail-x head-x
                       :tail-y (- tail-y 1)}))
      (update-state state
                    {:head-x head-x
                     :head-y new-head-y
                     :tail-x tail-x
                     :tail-y tail-y}))))

(defn move-left [{:keys [head tail] :as state}]
  (let [{head-x :x head-y :y} head
        {tail-x :x tail-y :y} tail
        new-head-x (- head-x 1)]
    (if (> (- tail-x new-head-x) 1)
      (if (= head-y tail-y)
        (update-state state
                      {:head-x new-head-x
                       :head-y head-y
                       :tail-x (- tail-x 1)
                       :tail-y tail-y})
        (update-state state
                      {:head-x new-head-x
                       :head-y head-y
                       :tail-x (- tail-x 1)
                       :tail-y head-y}))
      (update-state state
                    {:head-x new-head-x
                     :head-y head-y
                     :tail-x tail-x
                     :tail-y tail-y}))))

(defn parse-line [line]
  (let [[direction moves] (str/split line #" ")]
    {:direction direction
     :moves (read-string moves)}))

(defn make-move-with [f state moves]
  (reduce (fn [new-state _] (f new-state))
          state (repeat moves nil)))

(defn make-move [state {:keys [direction moves]}]
  (case direction
    "U" (make-move-with move-up state moves)
    "R" (make-move-with move-right state moves)
    "D" (make-move-with move-down state moves)
    "L" (make-move-with move-left state moves)))

(defn count-visited [lines]
  (->> lines
       (map parse-line)
       (reduce make-move starting-state)
       :visited
       count))

;;; Part 2

(defn plus [[x1 y1] [x2 y2]]
  [(+ x1 x2) (+ y1 y2)])

(defn minus [[x1 y1] [x2 y2]]
  [(- x1 x2) (- y1 y2)])

(def forces
  {[-2  0] [-1  0]
   [+2  0] [+1  0]
   [ 0 -2] [ 0 -1]
   [ 0 +2] [ 0 +1]

   [-2 -1] [-1 -1]
   [-2 +1] [-1 +1]
   [+2 -1] [+1 -1]
   [+2 +1] [+1 +1]
   [-1 -2] [-1 -1]
   [+1 -2] [+1 -1]
   [-1 +2] [-1 +1]
   [+1 +2] [+1 +1]
   [-2 -2] [-1 -1]
   [-2 +2] [-1 +1]
   [+2 -2] [+1 -1]
   [+2 +2] [+1 +1]})

(defn follow [[moved-head first-tail & more-tails :as rope]]
  (if-not first-tail
    rope
    (let [delta (minus moved-head first-tail)
          force (forces delta)]
      (if-not force
        rope
        (cons moved-head (follow (cons (plus first-tail force) more-tails)))))))


(defn parse-line [line]
  (let [[direction moves] (str/split line #" ")]
    (repeat (read-string moves)
            (case direction
              "U" [0 -1]
              "R" [+1 0]
              "L" [-1 0]
              "D" [0 +1]))))

(defn part2 [lines]
  (loop [[head & tails] (repeat 10 [0 0])
         visited #{}
         [direction & directions] (mapcat parse-line lines)]
    (let [visited (conj visited (last tails))]
      (if-not direction
        (count visited)
        (let [head (plus head direction)]
          (recur (follow (cons head tails)) visited directions))))))

(def test-input
  "R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2")

;; (u/run-with-string part2 test-input)

(u/run-with-file part2 "resources/day9.txt")
