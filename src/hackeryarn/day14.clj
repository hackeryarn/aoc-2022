(ns hackeryarn.day14
  (:require [clojure.string :as str]))

(defn parse-paths [input]
  (for [path (str/split input #"\n")]
    (for [[_ & xy] (re-seq #"(\d+),(\d+)" path)]
      (mapv read-string xy))))

(defn draw-line [grid [[x1 y1] [x2 y2]]]
  (let [dx (- x2 x1)
        dy (- y2 y1)]
    (loop [x x1
           y y1
           grid grid]
      (if (and (= x x2) (= y y2))
        (assoc grid (+ x (* y 1000)) true)
        (recur (cond
                 (neg? dx) (dec x)
                 (pos? dx) (inc x)
                 :else x)
               (cond
                 (neg? dy) (dec y)
                 (pos? dy) (inc y)
                 :else y)
               (assoc grid (+ x (* y 1000)) true))))))

(defn make-grid [paths]
  (let [grid (vec (repeat 1000000 false))]
    (reduce (fn [grid1 path]
             (reduce (fn [grid2 line] (draw-line grid2 line))
                     grid1 (partition 2 1 path)))
            grid paths)))

(defn drop-grain [grid]
  (loop [grain 500]
    (let [down (+ grain 1000)
          left (dec down)
          right (inc down)]
      (cond
        (>= down 1000000) false
        (false? (nth grid down)) (recur down)
        (false? (nth grid left)) (recur left)
        (false? (nth grid right)) (recur right)
        :else (assoc grid grain true)))))

(defn part1 [input]
  (let [paths (parse-paths input)
        grid (make-grid paths)]
    (loop [counter 0
           grid grid]
      (let [after-drop (drop-grain grid)]
        (if after-drop
          (recur (inc counter)
                 after-drop)
          counter)))))

;;; Part 2

(defn find-floor [paths]
  (let [y (->> (for [path paths
                     [x y] path]
                 y)
               (apply max)
               (+ 2))]
    [[0 y] [999 y]]))

(defn part2 [input]
  (let [paths (parse-paths input)
        floor (find-floor paths)
        grid (make-grid (cons floor paths))]
    (loop [counter 0
           grid grid]
      (if (nth grid 500)
        counter
        (recur (inc counter)
               (drop-grain grid))))))

(def test-input
  "498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9")

(part2 test-input)

(let [input (slurp "resources/day14.txt")]
  (time (part2 input)))
