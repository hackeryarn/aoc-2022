(ns hackeryarn.day4
  (:require [clojure.string :as str]
            [clojure.set :as set]
            [clojure.java.io :as io]))

;;; Part 1

(defn assignment->set [assignment]
  (->> (str/split assignment #"-")
       (map read-string)
       ((fn [[x y]] [x (+ y 1)]))
       (apply range)
       set))

(defn fully-contained? [assignments]
  (->> (str/split assignments #",")
       (map assignment->set)
       ((fn [[x y]] (or (set/superset? x y)
                       (set/superset? y x))))))

(defn count-fully-contained [assignments]
  (->> assignments
       (map fully-contained?)
       (filter true?)
       count))

;;; Part 2

(defn intersecting? [assignments]
  (->> (str/split assignments #",")
       (map assignment->set)
       (apply set/intersection)))

(defn count-intersecting [assignments]
  (->> assignments
       (map intersecting?)
       (filter (comp not empty?))
       count))

;; (def test-input
;;   "2-4,6-8
;; 2-3,4-5
;; 5-7,7-9
;; 2-8,3-7
;; 6-6,4-6
;; 2-6,4-8")

;; (-> test-input str/split-lines seq count-intersecting)

(time (with-open [rdr (io/reader "resources/day4.txt")]
        (count-intersecting (line-seq rdr))))
