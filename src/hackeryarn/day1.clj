(ns hackeryarn.day1
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(defn calc-total-cals ^Integer [cals]
  (->> cals
      (map #(Integer/parseInt %))
      (apply +)))

(defn most-cals [cals]
  (->> cals
       (map calc-total-cals)
       (apply (partial sorted-set-by >))
       (take 3)
       (apply +)))

(defn lines->cals [lines]
  (->> lines
      (partition-by str/blank?)
      (filter #(-> % first str/blank? not))))

(time (with-open [rdr (io/reader "resources/day1.txt")]
        (most-cals (lines->cals (line-seq rdr)))))
