(ns hackeryarn.day3
  (:require [clojure.string :as str]
            [clojure.set :as set]
            [clojure.java.io :as io]))

;;; Part 1

(defn split-str-in-half [s]
  (let [half (/ (count s) 2)]
    [(take half s) (drop half s)]))

(defn toggle-char-case [c]
  (if (Character/isUpperCase c)
    (Character/toLowerCase c)
    (Character/toUpperCase c)))

(defn char->priority [c]
  (let [initial (- (-> c toggle-char-case int) 64)]
    (if (>= initial 27)
      (- initial 6)
      initial)))

(defn calc-priority [strs]
  (->> strs
       (map set)
       (apply set/intersection)
       first
       char->priority))

(defn sum-priorities [items-strs]
  (->> items-strs
       (map split-str-in-half)
       (map calc-priority)
       (apply +)))

;;; Part 2

(defn sum-group-priorities [items-strs]
  (->> items-strs
       (partition 3)
       (map calc-priority)
       (apply +)))

(time (with-open [rdr (io/reader "resources/day3.txt")]
        (sum-group-priorities (line-seq rdr))))
