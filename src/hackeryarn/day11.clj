(ns hackeryarn.day11
  (:require [clojure.string :as str]))

(defn parse-starting-items [s]
  (->> (re-seq #"\d+" s)
       (map read-string)
       vec))

(defn parse-operation [s]
  (let [[op val] (re-seq #"[\+|\*|\d|]+" s)]
    {:op op
     :val (if (nil? val) nil (read-string val))}))

(defn parse-digit [s]
  (->> (re-seq #"\d+" s)
       first
       read-string))

(defn parse-monkey [monkey-str]
  (let [[monkey starting-items operation divisible-by if-true if-false]
        (str/split monkey-str #"\n")]
    {:index (parse-digit monkey)
     :items (parse-starting-items starting-items)
     :operation (parse-operation operation)
     :divisible-by (parse-digit divisible-by)
     :if-true (parse-digit if-true)
     :if-false (parse-digit if-false)
     :inspected 0}))

(defn apply-operation [{:keys [op val]} item modulo-factor]
  (let [val (or val item)]
    (case op
      "+" (mod (+ val item) modulo-factor)
      "*" (mod (* val item) modulo-factor))))

(defn inspect [monkeys {:keys [index operation divisible-by if-true if-false]} item]
  (let [modulo-factor (->> monkeys (map :divisible-by) (apply *))
        new (apply-operation operation item modulo-factor)
        pass-to (if (= 0 (rem new divisible-by)) if-true if-false)]
    (-> monkeys
        (update-in [index :inspected] + 1)
        (update-in [pass-to :items] conj new)
        (update-in [index :items] #(vec (rest %))))))

(defn take-turn [monkeys monkey]
  (reduce (fn [monkeys item] (inspect monkeys monkey item))
          monkeys (:items monkey)))

(defn take-round [monkeys]
  (loop [idx 0
         monkeys monkeys]
    (if (= idx (count monkeys))
      monkeys
      (recur (+ 1 idx)
             (take-turn monkeys (monkeys idx))))))

(defn solve [input]
  (->> (str/split input #"\n\n")
       (map parse-monkey)
       vec
       (iterate take-round)
       (take 10001)
       last
       (map :inspected)
       (sort >)
       (take 2)
       (apply *)))

(def test-input
  "Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1")

;; (solve test-input)

(solve (slurp "resources/day11.txt"))
