(ns hackeryarn.day8
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [hackeryarn.utils :as u]))

(defn parse-line [line]
  (->> (str/split line #"")
       (map read-string)
       (mapv (fn [height] {:height height :visible false :scenic-score 1}))))

(defn mark-from-left [row]
  (let [len (count row)]
    (loop [max-seen 0
           idx 0
           row row]
      (if (>= idx len)
        row
        (let [current (nth row idx)
              height (:height current)]
          (if (or (> height max-seen) (zero? idx))
            (recur height (+ 1 idx) (assoc-in row [idx :visible] true))
            (recur max-seen (+ 1 idx) row)))))))

(defn transpose [coll]
  (apply mapv vector coll))

(defn traverse-trees [f trees]
  (->> trees
       (map f)
       (map #(-> % reverse vec))
       (map f)
       transpose
       (map f)
       (map #(-> % reverse vec))
       (map f)))

(defn find-visible [input]
  (->> input
       (map parse-line)
       (traverse-trees mark-from-left)
       flatten
       (filter :visible)
       count))

;;; Part 2

(defn update-scenic-score [row]
  (let [len (count row)]
    (loop [seen '()
           idx 0
           row row]
      (if (>= idx (- len 1))
        row
        (let [height (-> row (nth idx) :height)
              [scenic ignored] (split-with (partial > height) seen)]
          (when (= height 5))
          (if (zero? idx)
            (recur (cons height seen)
                   (+ 1 idx)
                   (assoc-in row [idx :scenic-score] 0))
            (recur (cons height seen)
                   (+ 1 idx)
                   (update-in row [idx :scenic-score] * (+ (if (empty? ignored) 0 1)
                                                           (count scenic))))))))))

(defn find-visible [input]
  (->> input
       (map parse-line)
       (traverse-trees update-scenic-score)
       flatten
       (map :scenic-score)
       (apply max)))

(def test-input
  "30373
25512
65332
33549
35390")

;; (-> test-input str/split-lines find-visible)

(u/with-line-seq "resources/day8.txt" find-visible)
