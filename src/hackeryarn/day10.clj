(ns hackeryarn.day10
  (:require [hackeryarn.utils :as u]
            [clojure.string :as str]))

(def initial-state
  {:cycle 0
   :x 1
   :to-add 0})

(defn parse-line [line]
  (let [[op val] (str/split line #" ")]
    (case op
      "noop" 0
      "addx" [0 (read-string val)])))

(defn unroll-input [input]
  (->> input
       (map parse-line)
       flatten))

(defn update-state [{:keys [to-add] :as state} val]
  (-> state
      (update :cycle + 1)
      (update :x + to-add)
      (assoc :to-add val)))

(defn filter-significant-cycles
 [cycles]
 (reduce (fn [acc idx] (conj acc (nth cycles idx)))
         [] (range 20 (count cycles) 40)))

(defn calc-signal-strength
  [{:keys [cycle x]}]
  (* cycle x))

(defn determine-signal-strength
  [input]
  (->> input
       unroll-input
       (reductions update-state initial-state)
       filter-significant-cycles
       (map calc-signal-strength)
       (apply +)))

;;; Part 2

(defn draw-line [idx cycles]
  (reduce (fn [acc {:keys [cycle x]}]
            (let [sprite [(- x 1) x (+ x 1)]
                  drawing (- cycle (* idx 40) 1)]
              (if (some #(= drawing %) sprite)
                (conj acc "#")
                (conj acc "."))))
          [] cycles))

(defn draw-lines
  [input]
  (->> input
       unroll-input
       (reductions update-state initial-state)
       rest
       (partition 40)
       (map-indexed draw-line)
       (map println)
       doall))

(u/run-with-file draw-lines "resources/day10.txt")
