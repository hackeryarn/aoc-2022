(ns hackeryarn.day5
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(defn add-crate [stack crate-chars]
  (let [crate-char (nth crate-chars 1)]
    (if (= crate-char \space)
      stack
      (conj stack crate-char))))

(defn crates->crates-board [width crates-str-seq]
  (reduce (fn [stacks crates-str]
            (loop [crate-chars (seq crates-str)
                   i 0
                   stacks stacks]
              (if (or (= i width) (empty? crate-chars))
                stacks
                (recur
                 (drop 4 crate-chars)
                 (+ i 1)
                 (update stacks i #(add-crate % crate-chars))))))
          (vec (repeat width []))
          crates-str-seq))

(defn parse-board [ship]
  (let [board-seq (str/split-lines (:board ship))
        board-width (-> board-seq last last (Character/digit 10))]
    (update ship :board
            (fn [board-str]
              (->> board-str
                   str/split-lines
                   butlast
                   (crates->crates-board board-width))))))

(defn parse-moves [ship]
  (update ship :moves
          (fn [moves-str]
            (->> moves-str
                 str/split-lines
                 (map #(re-seq #"\d+" %))
                 (map #(map read-string %))
                 (map #(zipmap [:move :from :to] %))))))

(defn make-move [board {:keys [move from to]}]
  (let [from (- from 1)
        to (- to 1)
        crates-to-move (take move (nth board from))]
    (-> board
        (update from #(drop move %))
        (update to #(concat crates-to-move %)))))

(defn make-moves [{:keys [board moves]}]
  (reduce make-move board moves))

(defn determine-top-crates [input]
  (->> (str/split input #"\n\n")
       (zipmap [:board :moves])
       parse-board
       parse-moves
       make-moves
       (map first)))

;; (def test-input
;;   "    [D]
;; [N] [C]
;; [Z] [M] [P]
;;  1   2   3

;; move 1 from 2 to 1
;; move 3 from 1 to 3
;; move 2 from 2 to 1
;; move 1 from 1 to 2")

;; (-> test-input determine-top-crates)

(time (determine-top-crates (slurp "resources/day5.txt")))
