(ns hackeryarn.utils
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(defn run-with-file [f path]
  (with-open [rdr (io/reader path)]
    (time (f (line-seq rdr)))))

(defn run-with-string [f s]
  (-> s str/split-lines f))
