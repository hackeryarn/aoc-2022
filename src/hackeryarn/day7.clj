(ns hackeryarn.day7
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(defn build-fp [path]
  (str/join "/" path))

(defn run-cd [fs cwd [dir]]
  (let [new-cwd (conj cwd dir)]
    (case dir
      "/" [(assoc fs dir (get fs dir 0)) ["/"]]
      ".." [fs (pop cwd)]
      [(assoc fs (build-fp new-cwd) (get fs dir 0)) new-cwd])))

(defn parse-command [fs cwd command]
  (case (first command)
    "cd" (run-cd fs cwd (rest command))
    [fs cwd]))

(defn file->map [[size name]]
  {:size (read-string size)
   :name name})

(defn recalculate-sizes [fs cwd size]
  (loop [fs fs
         dirs cwd]
    (if (empty? dirs)
      fs
      (recur (update fs (build-fp dirs) + size)
             (pop dirs)))))

(defn parse-file [fs cwd file]
  (->> file
       first
       read-string
       (recalculate-sizes fs cwd)))

(defn parse-line [fs cwd line]
  (let [words (str/split line #" ")
        first-word (first words)]
    (cond
      (= "$" first-word) (parse-command fs cwd (rest words))
      (= "dir" first-word) [fs cwd]
      :else [(parse-file fs cwd words) cwd])))

(defn parse-input [input]
  (loop [fs {}
         cwd []
         input input]
    (if (empty? input)
      fs
      (let [[fs cwd] (parse-line fs cwd (first input))]
        (recur fs cwd (rest input))))))

(defn sum-file-sizes [input]
  (->> input
       parse-input
       vals
       (filter #(<= % 100000))
       (apply +)))

;;; Part 2

(defn find-smallest-file-to-delete [dirs]
  (let [consumed (dirs "/")
        free (- 70000000 consumed)
        needed (- 30000000 free)]
    (->> dirs
         vals
         (filter #(>= % needed))
         (apply min))))

(defn free-up [input]
  (->> input
       parse-input
       find-smallest-file-to-delete))

;; (def test-input
;;   "$ cd /
;; $ ls
;; dir a
;; 14848514 b.txt
;; 8504156 c.dat
;; dir d
;; $ cd a
;; $ ls
;; dir e
;; 29116 f
;; 2557 g
;; 62596 h.lst
;; $ cd e
;; $ ls
;; 584 i
;; $ cd ..
;; $ cd ..
;; $ cd d
;; $ ls
;; 4060174 j
;; 8033020 d.log
;; 5626152 d.ext
;; 7214296 k")

;; (-> test-input str/split-lines free-up)

(with-open [rdr (io/reader "resources/day7.txt")]
  (time (free-up (line-seq rdr))))
