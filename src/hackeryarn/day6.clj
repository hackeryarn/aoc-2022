(ns hackeryarn.day6)

(defn find-marker [window-size s]
  (loop [windows (partition window-size 1 s)
         pos 0]
    (if (apply distinct? (first windows))
      (+ window-size pos)
      (recur (rest windows) (+ 1 pos)))))

;; (find-marker "nppdvjthqldpwncqszvftbrmjlhg")

(time (find-marker 14 (slurp "resources/day6.txt")))
