(ns hackeryarn.day2
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(def p1-plays
  {"A" :rock
   "B" :paper
   "C" :scissors})

(def p2-plays
  {"X" :rock
   "Y" :paper
   "Z" :scissors})

(defn str->round [s]
  (let [[p1 p2] (str/split s #" ")]
    [(p1-plays p1) (p2-plays p2)]))

(defn winner? [x y]
  (or
    (and (= x :rock) (= y :paper))
    (and (= x :paper) (= y :scissors))
    (and (= x :scissors) (= y :rock))))

(defn play-round [[p1 p2]]
  (cond (winner? p1 p2) 6
        (winner? p2 p1) 0
        :else 3))

(def choice-scores
  {:rock 1
   :paper 2
   :scissors 3})

(defn round->score [[p1 p2]]
  (let [win-score (play-round [p1 p2])
        choice-score (choice-scores p2)]
    (+ win-score choice-score)))

;; Part 2

(def decoded-needs
  {"X" :lose
   "Y" :draw
   "Z" :win})

(defn p2-str->round [s]
  (let [[oponent-choice need] (str/split s #" ")]
    [(p1-plays oponent-choice) (decoded-needs need)]))

(def need-scores
  {:win 6
   :draw 3
   :lose 0})

(def winners
  {:rock :paper
   :paper :scissors
   :scissors :rock})

(defn winners->scores [winners]
  (into {} (map (fn [[choice winner]]
                  [choice (choice-scores winner)])
                winners)))

(defn add-win-to-scores [win-key scores]
  (into {} (map (fn [[key val]] [key (+ val (need-scores win-key))]) scores)))

(def score-map
  {:win (add-win-to-scores :win (winners->scores winners))
   :draw (add-win-to-scores :draw (winners->scores
                                   (into {} (map vector (keys winners) (keys winners)))))
   :lose (add-win-to-scores :lose (-> winners clojure.set/map-invert winners->scores))})

(defn p2-round->score [[oponent-choice need]]
  (-> score-map need oponent-choice))

(defn calc-score [input]
  (->> input
      (map p2-str->round)
      (map p2-round->score)
      (apply +)))

(time (with-open [rdr (io/reader "resources/day2.txt")]
        (calc-score (line-seq rdr))))
